#> Makefile

##> compiler setting
FC     = mpif90
#FFLAGS = -O2 -mtune=native -march=native -std=legacy -Wno-missing-include-dirs
FFLAGS = -O2 -std=legacy -fbounds-check -fbacktrace -Wuninitialized -ffpe-trap=invalid,zero,overflow -Wno-missing-include-dirs
# CC = mpicc -std=c99
# CFLAGS = -fPIC -O2
# CPP    = -cpp

# monolis_utils library
MONOLIS_DIR= ./monolis_utils
MONOLIS_INC= -I $(MONOLIS_DIR)/include
MONOLIS_LIB= -L$(MONOLIS_DIR)/lib -lmonolis_utils

LIBS    = $(MONOLIS_LIB)
INCLUDE = -I ./include $(MONOLIS_INC)

##> option setting
ifdef FLAGS
	comma:= ,
	empty:=
	space:= $(empty) $(empty)
	DFLAGS = $(subst $(comma), $(space), $(FLAGS))

	ifeq ($(findstring DEBUG, $(DFLAGS)), DEBUG)
		FFLAGS  = -fPIC -O2 -std=legacy -fbounds-check -fbacktrace -Wuninitialized -ffpe-trap=invalid,zero,overflow -Wno-missing-include-dirs
		CFLAGS  = -fPIC -O2 -g -ggdb
	endif

	ifeq ($(findstring INTEL, $(DFLAGS)), INTEL)
		FC      = mpiifort
		FFLAGS  = -O2 -mtune=native -march=native -align array64byte -axCORE-AVX512
		MOD_DIR = -module ./include
	endif
endif


##> directory setting
MOD_DIR = -J ./include
BIN_DIR = ./bin
SRC_DIR = ./src
OBJ_DIR = ./obj

##> other commands
MAKE = make
CD   = cd
RM   = rm -r
AR   = - ar ruv

##> **********
##> target
TARGET1 = $(BIN_DIR)/mkgraph
TARGET2 = $(BIN_DIR)/mkdensity
TARGET3 = $(BIN_DIR)/treat_vtk

##> source file define
SRC1 = \
makegraph.f90

SRC2 = \
utils.f90 \
makedensity.f90

SRC3 = \
utils.f90 \
treat_vtk.f90

##> lib objs
SOURCES1 = $(addprefix $(SRC_DIR)/, $(SRC1))
OBJS1 = $(subst $(SRC_DIR), $(OBJ_DIR), $(SOURCES1:.f90=.o))


SOURCES2 = $(addprefix $(SRC_DIR)/, $(SRC2))
OBJS2 = $(subst $(SRC_DIR), $(OBJ_DIR), $(SOURCES2:.f90=.o))

SOURCES3 = $(addprefix $(SRC_DIR)/, $(SRC3))
OBJS3 = $(subst $(SRC_DIR), $(OBJ_DIR), $(SOURCES3:.f90=.o))

##> target
all: $(TARGET1) $(TARGET2) $(TARGET3)

$(TARGET1): $(OBJS1)
	$(FC) $(FFLAGS) -o $@ $(OBJS1) $(LIBS)

$(TARGET2): $(OBJS2)
	$(FC) $(FFLAGS) -o $@ $(OBJS2) $(LIBS)

$(TARGET3): $(OBJS3)
	$(FC) $(FFLAGS) -o $@ $(OBJS3) $(LIBS)

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.f90
	$(FC) $(FFLAGS) $(INCLUDE) $(MOD_DIR) -o $@ -c $<

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -o $@ -c $< $(LDFLAGS)


clean:
	$(RM) \
	./src/*.mod \
	./obj/*.o \
	./include/*.mod \
	./bin/*

.PHONY: clean