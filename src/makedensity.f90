program main
    use utils
    use mod_monolis_utils
    implicit none

    integer :: i, j, k

    integer(kint) ::  n_node, n
    real(kdouble), allocatable :: p(:,:)

    ! integer(kint) :: n_vertex
    ! integer(kint), allocatable :: vertex_id(:)
    ! integer(kint), allocatable :: index_graph(:)
    ! integer(kint), allocatable :: item_graph(:)

    character(100) fname_node, fname_density !, fname_graph,
    character(100) fname_vtu
    character(4) step

    real(8) max_x, max_y, max_z
    real(8) min_x, min_y, min_z
    real(8) width, ewidth
    integer :: n_particle ! 1辺の分割数
    real(8) :: r ! 粒子半径

    real(8) x, y, z

    ! 粒子数密度セルの頂点
    real(8), allocatable :: vertex_density(:,:)
    ! 粒子数密度
    real(8), allocatable :: density(:)
    ! 粒子数密度 の基準
    real(8) :: density_ref
    ! 重み関数（カーネル関数）
    real(8) :: weight

    ! 影響半径
    real(8) :: r_e

    ! k-d木、各粒子について作成
    type(monolis_kdtree_structure), allocatable :: monolis_kdtree(:)
    integer(kint), allocatable :: n_BB
    integer(kint), allocatable :: BB_id(:) ! 1スタート
    real(kdouble), allocatable :: BB(:,:) ! 1スタート
    real(kdouble) :: pos(3)

    ! コマンドライン引数の値
    type(arguments), allocatable :: arg(:)
    integer :: n_process
    integer :: n_step
    integer :: n_density ! Basilisk に統一するため 2 の累乗

    ! MPI用
    integer nsize, rank, comm

    call monolis_mpi_initialize()
    comm = monolis_mpi_get_global_comm()
    nsize = monolis_mpi_get_global_comm_size()
    rank =  monolis_mpi_get_global_my_rank()

    ! コマンドライン引数を読み込む
    call commandvalue(arg)
    read(arg(1)%v,*) n_step
    if(rank == 0) write(*,'(a, i0)') "Number of denisty : ", n_density
    read(arg(2)%v,*) n_process
    if(rank == 0) write(*,'(a, i0)') "Number of process   : ", n_process
    read(arg(3)%v,*) n_density
    if(rank == 0) write(*,'(a, i0)') "Number of denisty : ", n_density

    allocate(monolis_kdtree(0:n_step))

    ! ファイル入力
    do n = 21, n_step
        write(*, "(I4.4)") n
        write(step, "(I4.4)") n
        if(n_step == 1) then
            fname_node = "./models/node_graph/node.dat"
        else
            fname_node = "./models/node_graph/node_" // trim(step) // ".dat"
        endif
        call monolis_input_node(fname_node, n_node, p) ! たぶん1スタートになる

        ! fname_graph = "./output/graph.dat"
        ! call monolis_input_graph(fname_graph, n_vertex, vertex_id, index_graph, item_graph)

        write(*,*) "input node-data"

        ! 領域の最大・最小値を取得
        max_x = -100000; max_y = -100000; max_z = -100000
        min_x =  100000; min_y =  100000; min_z =  100000

        do i = 1, n_node
            ! 最大
            if(p(1, i) > max_x) max_x = p(1, i)
            if(p(2, i) > max_y) max_y = p(2, i)
            if(p(3, i) > max_z) max_z = p(3, i)
            ! 最小
            if(p(1, i) < min_x) min_x = p(1, i)
            if(p(2, i) < min_y) min_y = p(2, i)
            if(p(3, i) < min_z) min_z = p(3, i)
        enddo

        width = max((max_x - min_x), (max_y - min_y), (max_z - min_z))
        min_x = min_x - (width / 10.0d0)
        min_y = min_y - (width / 10.0d0)
        min_z = min_z - (width / 10.0d0)
        width = 1.2 * width
        ewidth = width / (n_density - 1)

        write(*,'(a, f15.6, a, f15.6)') "xmax = ", max_x, ", xmin = ", min_x
        write(*,'(a, f15.6, a, f15.6)') "ymax = ", max_y, ", ymin = ", min_y
        write(*,'(a, f15.6, a, f15.6)') "zmax = ", max_z, ", zmin = ", min_z


        ! 粒子数密度基準点作成、0スタート
        allocate(vertex_density(0:((n_density ** 3) - 1), 3))
        do i = 0, (n_density - 1)
            z = min_z + (i * ewidth)
            do j = 0, (n_density - 1)
                y = min_y + (j * ewidth)
                do k = 0, (n_density - 1)
                    x = min_x + (k * ewidth)

                    vertex_density(((n_density ** 2) * i) + (n_density * j) + k, 1) = x
                    vertex_density(((n_density ** 2) * i) + (n_density * j) + k, 2) = y
                    vertex_density(((n_density ** 2) * i) + (n_density * j) + k, 3) = z
                enddo
            enddo
        enddo

        ! k-d木作成、1スタート
        ! 各粒子について作成
        n_BB = n_node
        n_particle = (n_node ** (1.0d0/3.0d0))
        r = (1.0d0 / n_particle) / 2 ! 粒子半径
        r_e = r * 3.3d0 ! 影響半径
        allocate(BB_id(n_BB), BB(6, n_BB))
        do i = 1, n_node
            BB_id(i) = i
            BB(1, i) = p(1, i) - r_e
            BB(2, i) = p(1, i) + r_e
            BB(3, i) = p(2, i) - r_e
            BB(4, i) = p(2, i) + r_e
            BB(5, i) = p(3, i) - r_e
            BB(6, i) = p(3, i) + r_e
        enddo

        call monolis_kdtree_init_by_bb(monolis_kdtree(n), n_BB, BB_id, BB)

        ! 粒子数密度計算
        allocate(density(0:((n_density ** 3) - 1)))
        density = 0.0d0
        density_ref = -10000
        do i = 0, ((n_density ** 3) - 1)
            ! セル頂点を入力
            pos(1) = vertex_density(i, 1)
            pos(2) = vertex_density(i, 2)
            pos(3) = vertex_density(i, 3)
            call monolis_kdtree_get_bb_including_coordinates(monolis_kdtree(n), pos, n_BB, BB_id)
            do j = 1, n_BB
                if((((pos(1) - p(1, BB_id(j))) ** 2) + &
                    ((pos(2) - p(2, BB_id(j))) ** 2) + &
                    ((pos(3) - p(3, BB_id(j))) ** 2)) ** (1.0d0 / 2.0d0) <= r_e) then
                    weight = - (((((pos(1) - p(1, BB_id(j))) ** 2) + &
                                ((pos(2) - p(2, BB_id(j))) ** 2) + &
                                ((pos(3) - p(3, BB_id(j))) ** 2)) ** (1.0d0 / 2.0d0)) / r_e) + 1
                    ! write(*,*) weight
                    density(i) = density(i) + weight
                endif
            enddo
            if(density(i) >= density_ref) density_ref = density(i)
        enddo

        ! ファイル出力
        if(n_step == 1) then
            fname_density = "./models/density/density.csv"
            fname_vtu = "./models/density/density.vtu"
        else
            fname_density = "./models/density/density_" // trim(step) // ".csv"
            fname_vtu = "./models/density/density_" // trim(step) // ".vtu"
        endif

        open(11, file = fname_density, status = "replace")
        write(11, '(3a)') "x y z density"
        do i = 0, ((n_density ** 3) - 1)
            write(11, '(4f15.6)') vertex_density(i, 1:3), (density(i) / density_ref)
        enddo
        write(*,*) "output CSV-data"

        call output_vtk(fname_vtu, n_node, p, r)
        write(*,*) "output vtu-data"
        write(*,*) " "

        call monolis_kdtree_finalize(monolis_kdtree(n))
        deallocate(vertex_density, BB, density)

    enddo
    call monolis_mpi_finalize()

end program main