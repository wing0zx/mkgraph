module utils
    use, intrinsic :: iso_fortran_env
    implicit none

    type arguments
        character(:), allocatable :: v
    end type arguments

    contains

    subroutine commandvalue(arg) ! コマンドライン変数取得
        integer(int32) :: argc
        type(arguments), allocatable :: arg(:)

        argc = command_argument_count() ! 実行コマンドを含まない引数の個数
        allocate(arg(0:argc))          ! 実行コマンド+引数を格納するための文字列配列

        get_arguments: block
            integer(int32) :: n, length_nth_arg
            do n = 0, argc !!!!!!!!!!!!!!!!!!!!!! 0は実行コマンド
                ! 第n引数の長さを取得
                call get_command_argument(number=n, length=length_nth_arg)

                ! 第n引数の長さと同じ長さの文字列を割付
                allocate (character(length_nth_arg) :: arg(n)%v)

                ! 第n引数の値を文字列で取得
                call get_command_argument(number=n, value=arg(n)%v)
            end do
        end block get_arguments
    end subroutine commandvalue

    subroutine output_node(fname, n_node, p)
        integer n_node
        integer i
        real(8), allocatable :: p(:,:) ! 点群の座標
        character(100) fname

        open(30, file = fname, status='replace')
        !node
        write(30,"(i0)")n_node
        do i = 1, n_node
            write(30,"(1pe25.15,a,1pe25.15,a,1pe25.15)") p(1, i), " ", p(2, i), " ", p(3, i)
        enddo
        close(30)
    end subroutine output_node


    subroutine input_vtk(fname, n_node, points)
        implicit none
        character(100), intent(in) :: fname
        integer, intent(out) :: n_node
        real(8), allocatable, intent(out) :: points(:,:)
        character(100) :: ctemp
        integer :: i
        real :: x1, y1, z1, x2, y2, z2, x3, y3, z3

        open(unit=10, file=fname, status='old')

        ! Skip header lines
        do i = 1, 4
            read(10, '(a)') ctemp
        end do

        read(10, '(a)') ctemp
        read(ctemp(8:13), *) n_node

        write(*,*) n_node

        ! Allocate points array
        allocate(points(3, n_node))

        ! Read the points
        do i = 1, (n_node / 3)
            read(10, *) x1, y1, z1, x2, y2, z2, x3, y3, z3
            points(1, (3 * i) - 2) = x1
            points(2, (3 * i) - 2) = y1
            points(3, (3 * i) - 2) = z1
            points(1, (3 * i) - 1) = x2
            points(2, (3 * i) - 1) = y2
            points(3, (3 * i) - 1) = z2
            points(1, (3 * i)    ) = x3
            points(2, (3 * i)    ) = y3
            points(3, (3 * i)    ) = z3
        end do

        close(10)
    end subroutine input_vtk

    subroutine output_vtk(fname, numpoints, a, r)
        integer numpoints
        ! integer, allocatable :: p(:,:)
        integer :: i
        real(8), allocatable :: a(:,:) ! 座標
        real(8) r ! 粒子半径
        ! real(8), allocatable :: u(:,:)
        ! real(8), allocatable :: eps(:,:)
        ! real(8), allocatable :: sig(:,:)
        character(100) fname ! 拡張子なしファイル名

        open(10, file = fname , status = "replace")

        write(10,'(a)') "<?xml version=""1.0""?>"
        write(10,'(a)') "<VTKFile type=""UnstructuredGrid"" version=""1.0"">"
        write(10,'(a)') "<UnstructuredGrid>"

        write(10,'(a,i0,a,i0,a)') "<Piece NumberOfPoints=""", numpoints, """ NumberOfCells=""", numpoints, """>"

        write(10,'(a)') "<Points>" ! 節点
        write(10,'(a,i0,a)') "<DataArray type=""Float32"" NumberOfComponents=""", 3, """ format=""ascii"">" !2次元なので。。。→2だとうまく可視化できなかった
        do i = 1, numpoints
            write(10,*) a(1:3, i)
        enddo
        write(10,'(a)') "</DataArray>"
        write(10,'(a)') "</Points>"

        write(10,'(a)') "<Cells>" ! 要素
        write(10,'(a,i0,a)') "<DataArray type=""Int32"" Name=""connectivity"" format=""ascii"">" ! 要素ごとの節点番号
        do i = 1, numpoints ! 修正済、整数が左詰め・半角英数字を挟むようにした
            if (i /= numpoints) then
                write(10,'(i0, a)', advance = 'no') i - 1, " " !この記述でよい、可視化には0スタートである必要があるが、Fortranの特性上入力ファイルは１スタート
            else
                write(10,'(i0)') i - 1
            endif
        enddo
        write(10,'(a)') "</DataArray>"
        write(10,'(a,i0,a)') "<DataArray type=""Int32"" Name=""offsets"" format=""ascii"">"
        do i = 1, numpoints ! 修正済、整数が左詰め・半角英数字を挟むようにした
            if (i /= numpoints) then
                write(10,'(i0, a)', advance = 'no') i, " "
            else
                write(10,'(i0)') i
            endif
        enddo
        write(10,'(a)') "</DataArray>"
        write(10,'(a,i0,a)') "<DataArray type=""UInt8"" Name=""types"" format=""ascii"">" ! 要素の種別番号（四角形は4）
        do i = 1, numpoints
            if (i /= numpoints) then
                write(10,'(i0,a)', advance = 'no') 1, " " !advanceなくてもいいかも
            else
                write(10,'(i0)') 1
            endif
        enddo
        write(10,'(a)') "</DataArray>"
        write(10,'(a)') "</Cells>"

        write(10,'(a)') "<PointData>" ! 半径
        write(10,'(a,i0,a)') "<DataArray type=""Float32"" Name=""radius"" format=""ascii"">"
        do i = 1, numpoints
          write(10,*) r
        enddo
        write(10,'(a)') "</DataArray>"
        write(10,'(a)') "</PointData>"

        ! write(10,'(a)') "<CellData>"
        ! write(10,'(a,i0,a)') "<DataArray type=""Float32"" Name=""Strain"" NumberOfComponents=""", 9, """ format=""ascii"">" ! ひずみ
        ! do i = 1, numcells
        !   write(10,*) eps(i, 1), eps(i, 3), 0.0d0
        !   write(10,*) eps(i, 3), eps(i, 2), 0.0d0
        !   write(10,*) 0.0d0, 0.0d0, 0.0d0
        ! enddo
        ! write(10,'(a)') "</DataArray>"

        ! write(10,'(a,i0,a)') "<DataArray type=""Float32"" Name=""Stress"" NumberOfComponents=""", 9, """ format=""ascii"">" ! 応力
        ! do i = 1, numcells
        !   write(10,*) sig(i, 1), sig(i, 3), 0.0d0
        !   write(10,*) sig(i, 3), sig(i, 2), 0.0d0
        !   write(10,*) 0.0d0, 0.0d0, 0.0d0
        ! enddo
        ! write(10,'(a)') "</DataArray>"
        ! write(10,'(a)') "</CellData>"

        write(10,'(a)') "</Piece>"
        write(10,'(a)') "</UnstructuredGrid>"
        write(10,'(a)') "</VTKFile>"
        close(10)
    end subroutine output_vtk
end module utils