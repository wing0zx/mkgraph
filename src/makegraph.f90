program main
    use mod_monolis_utils
    implicit none

    integer :: n_particle, n_all_particle
    integer :: i, j, k, n, nn
    real(8) :: x, y, z
    real(8) :: x_min, y_min, z_min
    real(8) :: r ! 粒子半径
    real(8) :: r_e ! 影響半径
    real(8), allocatable :: p(:,:)
    integer, allocatable :: id_connect(:)
    character(100) fname_node, fname_graph

    ! k-d木
    type(monolis_kdtree_structure) :: monolis_kdtree
    integer(kint), allocatable :: n_BB
    integer(kint), allocatable :: BB_id(:) ! 1スタート
    real(kdouble), allocatable :: BB(:,:) ! 1スタート
    real(kdouble) :: pos(3)

    write(*,'(a)') "Input number of nodes per side : "
    read(*,*) n_particle

    n_all_particle = n_particle ** 3

    n_BB = n_all_particle
    allocate(BB_id(n_BB), BB(6, n_BB))

    allocate(p(3, 0:(n_all_particle - 1))) ! (粒子数, 座標系)

    r = (1.0d0 / n_particle) / 2 ! 粒子半径
    r_e = r * 3.3d0 ! 影響半径

    x_min = -0.50d0 + r
    y_min = -0.50d0 + r
    z_min = -0.50d0 + r

    ! 今回は立方体格子状
    n = 1
    do i = 0, (n_particle - 1)
        z = z_min  + (i * (2 * r))
        do j = 0, (n_particle - 1)
            y = y_min + (j * (2 * r))
            do k = 0, (n_particle - 1)
                x = x_min + (k * (2 * r))

                p(1, n_particle * n_particle * i + n_particle * j + k) = x
                p(2, n_particle * n_particle * i + n_particle * j + k) = y
                p(3, n_particle * n_particle * i + n_particle * j + k) = z

                BB_id(n) = n
                BB(1, n) = x - r_e
                BB(2, n) = x + r_e
                BB(3, n) = y - r_e
                BB(4, n) = y + r_e
                BB(5, n) = z - r_e
                BB(6, n) = z + r_e
                n = n + 1
            enddo
        enddo
    enddo

    call monolis_kdtree_init_by_bb(monolis_kdtree, n_BB, BB_id, BB)

    fname_node = "./models/node_graph/node.dat"
    open(11, file = fname_node, status = "replace")
    write(11, '(i0)') n_all_particle
    do i = 0, (n_all_particle - 1)
        write(11, '(3f15.6)') p(1, i), p(2, i), p(3, i)
    enddo

    ! fname_graph = "./models/density/graph.dat"
    ! open(10, file = fname_graph, status = "replace")
    ! write(10, '(i0)') n_all_particle
    ! do i = 0, (n_all_particle - 1)
    !     pos(1) = p(1, i)
    !     pos(2) = p(2, i)
    !     pos(3) = p(3, i)
    !     call monolis_kdtree_get_bb_including_coordinates(monolis_kdtree, pos, n_BB, BB_id)
    !     allocate(id_connect(n_BB))
    !     nn = 1
    !     do j = 1, n_BB
    !         if((BB_id(j) - 1) /= i) then
    !             if((((pos(1) - p(1, (BB_id(j) - 1))) ** 2) + &
    !                 ((pos(2) - p(2, (BB_id(j) - 1))) ** 2) + &
    !                 ((pos(3) - p(3, (BB_id(j) - 1))) ** 2))  ** (1.0d0 / 2.0d0) <= r_e) then
    !                 id_connect(nn) = BB_id(j) - 1
    !                 nn = nn + 1
    !             endif
    !         endif
    !     enddo
    !     write(10, *) i, (nn - 1), id_connect(1:(nn - 1))
    !     deallocate(id_connect)
    ! enddo

    call monolis_kdtree_finalize(monolis_kdtree)

end program main