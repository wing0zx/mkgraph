program main
    use utils
    implicit none

    character(100) :: fname_vtk, fname_node
    character(4) :: step
    integer :: s

    integer :: n_node
    real(8), allocatable :: points(:,:)

    do s = 21, 40
        write(*,*) s
        write(step, "(I4.4)") s
        fname_vtk = "./models/vtk/PartFluid_" // trim(step) // ".vtk"
        write(*,*) fname_vtk
        call input_vtk(fname_vtk, n_node, points)

        fname_node = "./models/node_graph/node_" // trim(step) // ".dat"
        call output_node(fname_node, n_node, points)
    enddo
end program main
